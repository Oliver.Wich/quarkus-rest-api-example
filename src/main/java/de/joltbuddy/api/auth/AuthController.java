package de.joltbuddy.api.auth;

import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.security.AuthenticationFailedException;
import jakarta.annotation.security.PermitAll;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;
import org.eclipse.microprofile.jwt.JsonWebToken;

import java.util.List;
import java.util.NoSuchElementException;

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
public class AuthController {

    @Inject
    JsonWebToken jwt;

    @Inject
    UserRepository userRepository;

    @Inject
    TokenGenerator tokenGenerator;

    @GET
    @PermitAll
    public String getAuthStatus(@Context SecurityContext ctx) {
        return getResponseString(ctx);
    }

    private String getResponseString(SecurityContext ctx) {
        String name;
        if (ctx.getUserPrincipal() == null) {
            name = "anonymous";
        } else {
            name = ctx.getUserPrincipal().getName();
        }
        return String.format("hello + %s,"
                        + " groups: %s,"
                        + " isHttps: %s,"
                        + " authScheme: %s",
                name, jwt.getClaim("groups"), ctx.isSecure(), ctx.getAuthenticationScheme());
    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response register(UserDTO userDTO) {
        User user;

        try {
            user = userRepository.getByUsername(userDTO.getUsername());
            if (user != null) throw new EntityExistsException("User exists");
        } catch (EntityExistsException e) {
            return Response.status(409).build();
        } catch (NoSuchElementException ignored) {}

        userRepository.create(userDTO.getUsername(), userDTO.getPassword());

        return Response.status(201).build();
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(UserDTO userDTO) {
        User user;

        if (userDTO.getPassword() == null)
            return Response.status(401).build();

        try {
            user = userRepository.getByUsername(userDTO.getUsername());
            if (user == null) throw new NoSuchElementException();
        } catch (NoSuchElementException | AuthenticationFailedException e) {
            return Response.status(401).build();
        }

        if (!BcryptUtil.matches(userDTO.getPassword(), user.getPassword()))
            return Response.status(401).build();

        String authToken = tokenGenerator.generateAdminToken(user.getUsername());
        LoginResponse response = new LoginResponse(authToken);
        return Response.ok(response).build();
    }

    @GET
    @Path("/users")
    @RolesAllowed("ADMIN")
    public List<User> allUsers() {
        return userRepository.getAll();
    }
}
