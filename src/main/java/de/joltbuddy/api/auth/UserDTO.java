package de.joltbuddy.api.auth;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDTO {

    private String username;

    private String password;

    private String role;

    public UserDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
