package de.joltbuddy.api.auth;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LoginResponse {

    private boolean success = true;

    private String token;

    public LoginResponse(String token) {
        this.token = token;
    }
}
