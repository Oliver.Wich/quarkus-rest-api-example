package de.joltbuddy.api.core;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import jakarta.transaction.Transactional;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractRepository<E> {

    @Inject
    EntityManager entityManager;

    Class<E> relevantClass;

    public AbstractRepository(Class<E> relevantClass) {
        this.relevantClass = relevantClass;
    }

    public E getById(Long id) {
        return this.entityManager.find(relevantClass, id);
    }

    @Transactional
    protected Boolean deleteById(Long id) {
        try {
            E entity = getById(id);
            entityManager.remove(entity);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public List<E> getAll() {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<E> cq = cb.createQuery(relevantClass);
        Root<E> rootEntry = cq.from(relevantClass);
        CriteriaQuery<E> all = cq.select(rootEntry);
        TypedQuery<E> allQuery = this.entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    @Transactional
    protected void createNew(E object) throws RepositoryException {
        if (object.getClass() != relevantClass) {
            throw new RepositoryException("Can't persist Object of class " + object.getClass() + " in repo for " + relevantClass);
        }

        this.entityManager.persist(object);
    }

    @Transactional
    protected void deleteAll() {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
        CriteriaDelete<E> query = cb.createCriteriaDelete(relevantClass);
        query.from(relevantClass);
        Query allQuery = this.entityManager.createQuery(query);
        allQuery.executeUpdate();
    }
}
