package de.joltbuddy.api;

import de.joltbuddy.api.auth.LoginResponse;
import de.joltbuddy.api.auth.UserDTO;
import de.joltbuddy.api.auth.UserRepository;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.response.Response;
import jakarta.inject.Inject;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.*;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class AuthControllerTest {

    static final String testUsername = "TestUser" + RandomStringUtils.randomAlphabetic(5);
    static final String testUsernameTwo = "TestUser" + RandomStringUtils.randomAlphabetic(5) + "_new";
    static final String testPassword = "TestPassword" + RandomStringUtils.randomAlphabetic(16);
    @Inject
    UserRepository userRepository;

    @BeforeEach
    void createTestUser() {
        userRepository.create(testUsername, testPassword);
    }

    @AfterEach
    void deleteTestUsers() {
        userRepository.deleteByUsername(testUsername);
        userRepository.deleteByUsername(testUsernameTwo);
    }

    @Test
    public void testAuthEndpointAnonymous() {
        Response response = given()
                .when().get("/auth")
                .then()
                .extract().response();

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals("hello + anonymous, groups: null, isHttps: false, authScheme: null", response.asString());
    }

    @Test
    public void testUserEndpointAnonymous() {
        Response response = given()
                .when().get("/auth/users")
                .then()
                .extract().response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    @TestSecurity(user = "TestUser", roles = "OTHER")
    public void testUserEndpointOther() {
        Response response = given()
                .when().get("/auth/users")
                .then()
                .extract().response();

        Assertions.assertEquals(403, response.statusCode());
    }

    @Test
    @TestSecurity(user = "TestUser", roles = "ADMIN")
    public void testUserEndpointAdmin() {
        Response response = given()
                .when().get("/auth/users")
                .then()
                .extract().response();

        Assertions.assertEquals(200, response.statusCode());
    }


    @Test
    public void testWrongPasswordLogin() {
        UserDTO user = new UserDTO(testUsername, "wrong");

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(user)
                .when().post("/auth/login")
                .then()
                .extract().response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    public void testMissingPasswordLogin() {
        UserDTO user = new UserDTO();
        user.setUsername(testUsername);

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(user)
                .when().post("/auth/login")
                .then()
                .extract().response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    public void testMissingUsernameLogin() {
        UserDTO user = new UserDTO();
        user.setPassword(testPassword);

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(user)
                .when().post("/auth/login")
                .then()
                .extract().response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    public void testAdminUserLogin() {
        UserDTO user = new UserDTO(testUsername, testPassword);

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(user)
                .when().post("/auth/login")
                .then()
                .extract().response();

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertTrue(response.as(LoginResponse.class).isSuccess());

        LoginResponse loginResponse = response.as(LoginResponse.class);

        // Test if token works
        Response usersResponse = getAuthUsersEndpointWithToken(loginResponse.getToken());
        Assertions.assertEquals(200, usersResponse.statusCode());
    }

    @Test
    public void testUserRegistrationExisting() {
        UserDTO newUser = new UserDTO(testUsername, testPassword);

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(newUser)
                .when().post("/auth/register")
                .then()
                .extract().response();

        Assertions.assertEquals(409, response.statusCode());
    }

    @Test
    public void testUserRegistrationNewName() {
        UserDTO newUser = new UserDTO(testUsernameTwo, testPassword);

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(newUser)
                .when().post("/auth/register")
                .then()
                .extract().response();

        Assertions.assertEquals(201, response.statusCode());

        // Now test if we can log in with the user
        Response loginTestResponse = given()
                .header("Content-type", "application/json")
                .and()
                .body(newUser)
                .when().post("/auth/login")
                .then()
                .extract().response();

        Assertions.assertEquals(200, loginTestResponse.statusCode());
        Assertions.assertTrue(loginTestResponse.as(LoginResponse.class).isSuccess());

        LoginResponse loginResponse = loginTestResponse.as(LoginResponse.class);

        // Test if token works
        Response usersResponse = getAuthUsersEndpointWithToken(loginResponse.getToken());
        Assertions.assertEquals(200, usersResponse.statusCode());
    }


    private Response getAuthUsersEndpointWithToken(String token) {
        return given()
                .header("Authorization", "Bearer " + token)
                .when().get("/auth/users")
                .then()
                .extract().response();
    }

}
