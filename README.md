[Qodana Code Report](https://oliver.wich.gitlab.io/quarkus-rest-api-example/)

# quarkus REST Api Example

Basic Rest Api Example with auth and Entity management

This project uses Quarkus, the Supersonic Subatomic Java Framework.
If you want to learn more about Quarkus, please visit its website: [quarkus.io](https://quarkus.io/).

## Setup
Copy `.env.example` to `.env` and fill the fields.

Generate a private and public key in `src/main/resources`:
```shell
openssl genrsa -out rsaPrivateKey.pem 2048
openssl rsa -pubout -in rsaPrivateKey.pem -out publicKey.pem

openssl pkcs8 -topk8 -nocrypt -inform pem -in rsaPrivateKey.pem -outform pem -out privateKey.pem
```

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

Or use the Quarkus CLI:
```shell script
quarkus dev
```


> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application
<details>
    <summary> Guide </summary>

    The application can be packaged using:
    >$ ./mvnw package

    It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
    Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.
    
    The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.
    
    If you want to build an _über-jar_, execute the following command:
    >$ ./mvnw package -Dquarkus.package.type=uber-jar
    
    The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

</details>

## Creating a native executable
<details>
    <summary> Guide </summary>
    
    You can create a native executable using:
    >$ ./mvnw package -Pnative
    
    Or, if you don't have GraalVM installed, you can run the native executable build in a container using:
    >$ ./mvnw package -Pnative -Dquarkus.native.container-build=true
    
    You can then execute your native executable with: `./target/o-wich-api-1.0.0-SNAPSHOT-runner`
    If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

</details>

## Other Quarkus Docs
<details>
    <summary> Expand </summary>

    Related Guides
    ==============

    - Hibernate ORM https://quarkus.io/guides/hibernate-orm: Define your persistent model with Hibernate ORM and JPA
    - RESTEasy JAX-RS https://quarkus.io/guides/rest-json: REST endpoint framework implementing JAX-RS and more

    Provided Code
    =============
    
    Hibernate ORM
    --------------
    
    Create your first JPA entity: https://quarkus.io/guides/hibernate-orm
    
    RESTEasy JAX-RS
    ---------------
    
    Easily start your RESTful Web Services: https://quarkus.io/guides/getting-started#the-jax-rs-resources

</details>
