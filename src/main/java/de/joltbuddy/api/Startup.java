package de.joltbuddy.api;

import de.joltbuddy.api.auth.UserRepository;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;


@Singleton
public class Startup {
    @Inject
    UserRepository userRepository;

    @ConfigProperty(name = "application.admin.username")
    String adminUsername;

    @ConfigProperty(name = "application.admin.password")
    String adminPassword;

    public void loadAdminUser(@Observes StartupEvent evt) {
        userRepository.deleteByUsername(adminUsername);
        userRepository.create(adminUsername, adminPassword);
    }
}
