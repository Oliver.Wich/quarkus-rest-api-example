package de.joltbuddy.api.auth;

import io.smallrye.jwt.build.Jwt;
import jakarta.enterprise.context.RequestScoped;

@RequestScoped
public class TokenGenerator {
    /**
     * Generate JWT token
     */
    public String generateAdminToken(String username) {
        return Jwt.issuer("joltbuddy.de/issuer")
                .upn(username)
                .groups("ADMIN").sign();
    }
}
