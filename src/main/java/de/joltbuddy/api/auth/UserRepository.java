package de.joltbuddy.api.auth;

import de.joltbuddy.api.core.AbstractRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import org.jboss.logging.Logger;

import java.util.NoSuchElementException;
import java.util.Optional;

@ApplicationScoped
public class UserRepository extends AbstractRepository<User> {

    private static final Logger logger = Logger.getLogger(UserRepository.class);

    public UserRepository() {
        super(User.class);
    }

    public User getByUsername(String username) {
        Optional<User> user = super.getAll().stream().filter(u -> u.getUsername().equals(username)).findFirst();
        return user.orElseThrow();
    }

    @Transactional
    public void deleteByUsername(String username) {
        User user;
        try {
            user = getByUsername(username);
        } catch (NoSuchElementException e) {
            logger.info("Cant find User with name: " + username);
            return;
        }
        super.deleteById(user.getId());
    }

    @Transactional
    public void deleteAll() {
        super.deleteAll();
    }


    @Transactional
    public User create(String username, String password) {
        if (username == null || username.equals("")) return null;
        if (password == null || password.equals("")) return null;

        User user = new User();

        user.setUsername(username);
        user.setPassword(password);

        try {
            super.createNew(user);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }

        return user;
    }

}
